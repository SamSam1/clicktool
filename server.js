/**
 * 
 * Market Place for ClickTool
 *
 * @author  SamSelcuk
 * @version V1
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const db = require('./api/modal/database.js');
const userController = require('./api/controller/userController');
const itemController = require('./api/controller/itemController');
const auctionController = require('./api/controller/auctionController');

/*
*=============================================
*    CONNECTING TO DATABASE
*=============================================
*/

const path = require('path');
require('dotenv').config();
// db
// 	.any('DROP TABLE IF EXISTS customer')
// 	.then(() => {
// 		db.any('DROP TABLE IF EXISTS item');
// 	})
// 	.then(() => {
// 		db.any('DROP TABLE IF EXISTS auction');
// 	})
// 	.then(() => {
// 		db
// 			.any('create TABLE customer (uid INT,email VARCHAR(255),password VARCHAR(255), primary key (uid))')
// 			.then(() => {
// 				db
// 					.any(
// 						'create TABLE item  (iid INT, itemName VARCHAR(255), itemDefaultPrice VARCHAR(255), soldPrice VARCHAR(255), primary key (iid))'
// 					)
// 					.then(() => {
// 						db
// 							.any(
// 								'create TABLE auction(aid INT, highBid DECIMAL, uid INT, iid INT, primary key (aid), FOREIGN KEY(uid) references customer(uid), FOREIGN KEY(iid) references item(iid))'
// 							)
// 							.then(() => {
// 								pgp.end();
// 							});
// 					});
// 			});
// 	});

// CORS
app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

// Use Statements
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Load environment variables from .env
var dotenv = require('dotenv');
dotenv.config();

/*
*=============================================
*    ROUTES HANDLES CRUD OPERATIONS
*=============================================
*/
//  test
app.get('/express_backend', (req, res) => {
	res.json({ express: 'I AM WOKRING' });
});

/*
*  USER ROUTES
*/
app.get('/getUser', userController.getUser);
app.post('/createUser', userController.createUser);
/*
*  ITEM ROUTES
*/
app.get('/getItem', itemController.getItem);
app.post('/addItem', itemController.addItem);
app.get('/getItemId', itemController.getItemId);

/*
*  AUCTION ROUTES
*/
app.get('/getAuction', auctionController.getAuction);

/*
*=============================================
*          PORT SETUP
*=============================================
*/
const port = process.env.PORT || 5000;

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));
