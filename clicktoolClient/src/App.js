import React from 'react';
import './App.css';
import Market from './components/Market';

function App() {
	return (
		<div className="App">
			<h1>Listing Products</h1>
			<Market />
		</div>
	);
}

export default App;
