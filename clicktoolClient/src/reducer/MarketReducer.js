import * as types from '../actions/actionTypes';
const initialState = {
	products: []
};

export default (state = initialState, action) => {
	switch (action.type) {
		case types.STORE_PRODUCTS:
			return {
				products: action.payload
			};
		default:
			return state;
	}
};
