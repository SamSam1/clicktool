import { createStore, applyMiddleware } from 'redux';
import Marketreducer from './reducer/MarketReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const store = createStore(Marketreducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;
