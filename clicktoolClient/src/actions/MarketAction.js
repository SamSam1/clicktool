import * as types from './actionTypes';
import store from '../store';

export const fetchProducts = () => {
	return (dispatch, getState) => {
		fetch('http://localhost:5000/getitem').then((data) => data.json()).then((result) => {
			dispatch(storeProducts(result));
		});
	};
};
export const addProducts = (e) => {
  const obj = {
    itemName:e.target.itemName.value,
    itemDefaultPrice:e.target.itemDefaultPrice.value,
    soldPrice:e.target.soldPrice.value,
  }
	return (dispatch, getState) => {
    fetch('http://localhost:5000/addItem',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(obj)
    })
    .then((data) => data.json())
    .then((result) => fetchProducts())
  }
}
export const storeProducts = (products) => ({
	type: types.STORE_PRODUCTS,
	payload: products
});
