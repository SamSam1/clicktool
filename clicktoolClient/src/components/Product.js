import React, { Component } from 'react';

const Product = (props) => {
	return (
		<div>
			<p>{props.itemName}</p>
			<p>{props.itemDefaultPrice}</p>
		</div>
	);
};

export default Product;
