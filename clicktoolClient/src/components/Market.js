import React, { Component } from 'react';
import Product from './Product.js';
import { fetchProducts } from '../actions/MarketAction.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './../actions/MarketAction';

class Market extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		// console.log(this.props.fetchProducts)
		this.props.fetchProducts();
	}

	render() {
		let selected = [];
		this.props.products.forEach((element) => {
			selected.push(
				<div>
					<Product itemName={element.itemname} itemDefaultPrice={element.itemdefaultprice} />
				</div>
			);
		});
		return (
			<React.Fragment>
				<form onSubmit={this.props.addProducts}>
					<label>
						Add Item:
						<input type="text" value="iid" name="iid" />
						<input type="text" value="iid" name="itemName" />
						<input type="text" value="iid" name="itemDefaultPrice" />
						<input type="text" value="iid" name="soldPrice" />
					</label>
					<input type="submit" value="Submit" />
				</form>
				{selected}
			</React.Fragment>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators(
		{
			fetchProducts: actions.fetchProducts
		},
		dispatch
	);
};

const mapStateToProps = (state) => {
	return {
		products: state.products
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Market);
