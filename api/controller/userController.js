const db = require('./../modal/database.js');
const user = {};

user.getUser = (req, res, next) => {
	db
		.any('SELECT * FROM customer')
		.then((data) => {
			res.status(200).send(data);
		})
		.catch((err) => {
			console.log(err);
		});
};

user.createUser = (req, res, next) => {
	const { uid, email, password } = req.body;
	const values = [ uid, email, password ];
	db
		.any('INSERT INTO customer(uid,email,password) VALUES($1,$2,$3) RETURNING uid,email,password', values)
		.then((data) => {
			res.json(data);
		})
		.catch((error) => {
			console.log('ERROR:', error); // print error;
		});
};

module.exports = user;
