const db = require('./../modal/database.js');
const item = {};

item.getItem = (req, res, next) => {
	db
		.any('SELECT iid,itemName,itemDefaultPrice,soldPrice FROM "item"')
		.then((data) => {
			res.status(200).send(data);
		})
		.catch((err) => {
			console.log(err);
		});
};

item.addItem = (req, res, next) => {
	const { iid, itemName, itemDefaultPrice, soldPrice } = req.body;
	const values = [ iid, itemName, itemDefaultPrice, soldPrice ];
	db
		.one(
			'INSERT INTO item(iid,itemName,itemDefaultprice,soldPrice) VALUES($1, $2, $3, $4) RETURNING  iid,itemName,itemDefaultPrice,soldPrice',
			values
		)
		.then((data) => {
			res.status(200).send(data);
		})
		.catch((error) => {
			console.log(error);
		});
};

item.getItemId = (req, res, next) => {
	const { iid } = req.body;
	db
		.any('SELECT * FROM item WHERE iid = $1', iid)
		.then((data) => {
			res.status(200).send(data);
		})
		.catch((error) => {
			console.log(error);
		});
};

module.exports = item;
