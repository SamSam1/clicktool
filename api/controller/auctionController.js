const db = require('./../modal/database.js');
const auction = {};

auction.getAuction = (req, res, next) => {
	db
		.any('SELECT aid, highBid, uid, iid FROM "auction"')
		.then((data) => {
			res.status(200).send(data);
		})
		.catch((err) => {
			console.log(err);
		});
};

module.exports = auction;
